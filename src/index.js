import React from "react";
import ReactDom from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle";
import styles from "./css/style.css";
ReactDom.render(
  <div>
    <div className="container">
      <div className="headerSec">
        <div className="brand">Cootels</div>
        <ul>
          <li>
            <a href="#">Rooms</a>
          </li>
          <li>
            <a href="#">Reservation</a>
          </li>
          <li>
            <a href="#">Contacs</a>
          </li>
        </ul>
        <button className="headerBtn">Get Started</button>
      </div>

      <div className="row row1">
        <div className="col-xl-6">
          <div className="main">
            <div className="title-header">
              Nature, Warmth, and Beauty in One Space
            </div>
            <div className="title-body">
              One place to release all the stress, bring back happines, and get
              back to nature. We provide the best room and nature for you. Come
              visit us anytime you want.
            </div>

            <button className="btnFooter">Reservation</button>
          </div>
        </div>
        <div className="col-xl-6">
          <div className="imgSec">
            <img src="images/img1.png" alt="img1"></img>
            <img src="images/img2.png" alt="img2"></img>
            <img src="images/img3.png" alt="img3"></img>
          </div>
        </div>
      </div>

      <div className="row row2 mb-5">
        <div className="col-xl-5">
          <img src="images/img4.png" alt=""></img>
        </div>
        <div className="col-xl-7">
          <div>
            <div className="title2">
              Cozy and Down to Earth Cootage Hotel in Norway.
            </div>
            <div className="text2">
              Our Cootage Hotel is an intimate hideway concieved for dicerning
              travelers. It faces directly the unique and spectacular panorama
              of the Hallerbos Jungle.
            </div>
            <div className="text2 texttop">
              Cootels extends along a private, quiet, and beautiful nature. Stay
              away from crowd and enjoy the cozy and beauty with us.
            </div>
            <button className="mybtn2">Learn More</button>
          </div>
        </div>
      </div>

      <div className="row row3 mt-5">
        <div className="col-xl-6">
          <div>
            <div className="title3">Cabin Activities</div>
            <div className="text3">
              Don’t worry to get bored easily in our cabin. We have so many
              cabin activities for you to do it alone or with group. Maybe this
              is the best chance for you to make new friends or even a lover.
            </div>
          </div>
        </div>
        <div className="col-xl-6 text-center">
          <img className="img-fluid img4" src="images/img5.png" alt=""></img>
        </div>
      </div>
      <div className="row mt-5 rowl">
        <div className="col-xl-6">
          <img className="img-fluid img4" src="images/img6.png" alt=""></img>
        </div>
        <div className="col-xl-6 text-start">
          <div>
            <div className="title3">100% Organic Food</div>
            <div className="text3">
              We served 100% organic, low process and delicious food. We allow
              our guest to have breakfast or dinner request.What ever made your
              taste buds happy.
            </div>
          </div>
        </div>
      </div>

      <div className="row rowcenter">
        <div className="col-xl-12">
          <div className="text-center">
            <div className="title4">Many Rooms to Choose</div>
            <div className="text4">
              There is a room for every needs. We have room for individuals
              until family size, we also have a cabin for more private
              experience
            </div>
            <button className="btn4">Explore more</button>
          </div>
        </div>
      </div>

      <div className="row rowcol4">
        <div className="col-xl-4">
          <div className="card">
            <div className="card-image">
              <img
                className="img-fluid w-100"
                src="images/img7.png"
                alt=""
              ></img>
            </div>
            <div className="card-body">
              <div className="card-title">Single Room</div>
              <div className="card-text">
                Best for a brave lone wolf who need comfort and quiet quality
                time, but you still have a chance to meet others.
              </div>
              <div className="learnmore d-flex">
                <div>
                  <a href="#">Learn more</a>
                </div>
                <img src="images/arrow.png" alt=""></img>
              </div>
            </div>
          </div>
        </div>

        <div className="col-xl-4">
          <div className="card">
            <div className="card-image">
              <img
                className="img-fluid w-100"
                src="images/img8.png"
                alt=""
              ></img>
            </div>
            <div className="card-body">
              <div className="card-title">Double Room</div>
              <div className="card-text">
                Best for a brave lone wolf who need comfort and quiet quality
                time, but you still have a chance to meet others.
              </div>
              <div className="learnmore d-flex">
                <div>
                  <a href="#">Learn more</a>
                </div>
                <img src="images/arrow.png" alt=""></img>
              </div>
            </div>
          </div>
        </div>

        <div className="col-xl-4">
          <div className="card">
            <div className="card-image">
              <img
                className="img-fluid w-100"
                src="images/img9.png"
                alt=""
              ></img>
            </div>
            <div className="card-body">
              <div className="card-title">Cootage</div>
              <div className="card-text">
                Best for a brave lone wolf who need comfort and quiet quality
                time, but you still have a chance to meet others.
              </div>
              <div className="learnmore d-flex">
                <div>
                  <a href="#">Learn more</a>
                </div>
                <img src="images/arrow.png" alt=""></img>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="row row4">
        <div className="col-xl-6">
          <div>
            <div className="start">Start your journey!</div>
            <div className="title">How to Get My Room?</div>
            <div className="text">
              You can contact us by phone or email us. Easily tap the contact
              button below and it will take you to our contact point
            </div>
            <div className="footersec d-flex">
              <button className="contactBtn">Contact us</button>
              <div>Explore more room</div>
            </div>
          </div>
        </div>
        <div className="col-xl-6">
          <div class="accordion" id="accordionExample">
            <div class="accordion-item">
              <button
                class="accordion-button"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapseOne"
                aria-expanded="true"
                aria-controls="collapseOne"
              >
                1. Choose a room and date
              </button>
              <div
                id="collapseOne"
                class="accordion-collapse collapse show"
                aria-labelledby="headingOne"
                data-bs-parent="#accordionExample"
              >
                <div class="accordion-body">
                  Contact us via phone or email, ask for availability of you
                  choice of room. We might offer you something exciting.
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <button
                class="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapseTwo"
                aria-expanded="false"
                aria-controls="collapseTwo"
              >
                2. Check for room availability
              </button>
              <div
                id="collapseTwo"
                class="accordion-collapse collapse"
                aria-labelledby="headingTwo"
                data-bs-parent="#accordionExample"
              >
                <div class="accordion-body">
                  Contact us via phone or email, ask for availability of you
                  choice of room. We might offer you something exciting.
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <button
                class="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapseThree"
                aria-expanded="false"
                aria-controls="collapseThree"
              >
                3. Enjoy your room
              </button>
              <div
                id="collapseThree"
                class="accordion-collapse collapse"
                aria-labelledby="headingThree"
                data-bs-parent="#accordionExample"
              >
                <div class="accordion-body">
                  Contact us via phone or email, ask for availability of you
                  choice of room. We might offer you something exciting.
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="row row5">
        <div
          id="carouselExampleDark"
          class="carousel carousel-dark slide"
          data-bs-ride="carousel"
        >
          <div class="carousel-indicators">
            <button
              type="button"
              data-bs-target="#carouselExampleDark"
              data-bs-slide-to="0"
              class="active"
              aria-current="true"
              aria-label="Slide 1"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleDark"
              data-bs-slide-to="1"
              aria-label="Slide 2"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleDark"
              data-bs-slide-to="2"
              aria-label="Slide 3"
            ></button>
          </div>
          <div class="carousel-inner">
            <div class="carousel-item active" data-bs-interval="10000">
              <div className="title">Hear From Our Happy Customers</div>
              <img src="images/img10.png" alt=""></img>
              <div className="text">
                “Great service, great food, great people. The scenery is also
                beautiful, you can do so much activity even with your famility.
                Worth every penny!you should come and see for your self.5 out of
                5!the waffle is great!”
              </div>
              <div className="name">Kirana Dunham</div>
            </div>
            <div class="carousel-item" data-bs-interval="2000">
              <div className="title">Hear From Our Happy Customers</div>
              <img src="images/img10.png" alt=""></img>
              <div className="text">
                “Great service, great food, great people. The scenery is also
                beautiful, you can do so much activity even with your famility.
                Worth every penny!you should come and see for your self.5 out of
                5!the waffle is great!”
              </div>
              <div className="name">Kirana Dunham</div>
            </div>
            <div class="carousel-item">
              <div className="title">Hear From Our Happy Customers</div>
              <img src="images/img10.png" alt=""></img>
              <div className="text">
                “Great service, great food, great people. The scenery is also
                beautiful, you can do so much activity even with your famility.
                Worth every penny!you should come and see for your self.5 out of
                5!the waffle is great!”
              </div>
              <div className="name">Kirana Dunham</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer>
      <div className="container">
        <div className="row row6">
          <div className="col-xl-3">
            <div className="cootelsSec">
              <div className="title">Cootels</div>
              <div className="text">
                Your Best Private Hideway From Crowd. Back to Nature.
              </div>
              <div className="links">
                <svg
                  className="twitter"
                  width="32"
                  height="26"
                  viewBox="0 0 32 26"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M32 3.078C30.81 3.6 29.542 3.946 28.22 4.114C29.58 3.302 30.618 2.026 31.106 0.488C29.838 1.244 28.438 1.778 26.946 2.076C25.742 0.794 24.026 0 22.154 0C18.522 0 15.598 2.948 15.598 6.562C15.598 7.082 15.642 7.582 15.75 8.058C10.296 7.792 5.47 5.178 2.228 1.196C1.662 2.178 1.33 3.302 1.33 4.512C1.33 6.784 2.5 8.798 4.244 9.964C3.19 9.944 2.156 9.638 1.28 9.156C1.28 9.176 1.28 9.202 1.28 9.228C1.28 12.416 3.554 15.064 6.536 15.674C6.002 15.82 5.42 15.89 4.816 15.89C4.396 15.89 3.972 15.866 3.574 15.778C4.424 18.376 6.836 20.286 9.704 20.348C7.472 22.094 4.638 23.146 1.57 23.146C1.032 23.146 0.516 23.122 0 23.056C2.906 24.93 6.35 26 10.064 26C22.136 26 28.736 16 28.736 7.332C28.736 7.042 28.726 6.762 28.712 6.484C30.014 5.56 31.108 4.406 32 3.078Z" />
                </svg>
                <svg
                  className="instagram"
                  width="32"
                  height="32"
                  viewBox="0 0 32 32"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M23.1688 0H8.8306C3.96141 0 0 3.9616 0 8.83079V23.169C0 28.0384 3.96141 31.9998 8.8306 31.9998H23.1688C28.0384 31.9998 31.9998 28.0382 31.9998 23.169V8.83079C32 3.9616 28.0384 0 23.1688 0ZM29.1608 23.169C29.1608 26.4729 26.4729 29.1606 23.169 29.1606H8.8306C5.52693 29.1608 2.83918 26.4729 2.83918 23.169V8.83079C2.83918 5.52712 5.52693 2.83918 8.8306 2.83918H23.1688C26.4727 2.83918 29.1606 5.52712 29.1606 8.83079V23.169H29.1608Z" />
                  <path d="M15.9999 7.75482C11.4533 7.75482 7.75439 11.4537 7.75439 16.0004C7.75439 20.5468 11.4533 24.2455 15.9999 24.2455C20.5466 24.2455 24.2455 20.5468 24.2455 16.0004C24.2455 11.4537 20.5466 7.75482 15.9999 7.75482ZM15.9999 21.4062C13.019 21.4062 10.5936 18.9811 10.5936 16.0002C10.5936 13.019 13.0188 10.5938 15.9999 10.5938C18.9811 10.5938 21.4063 13.019 21.4063 16.0002C21.4063 18.9811 18.9809 21.4062 15.9999 21.4062Z" />
                  <path d="M24.5913 5.34735C24.0443 5.34735 23.507 5.56881 23.1206 5.95683C22.7324 6.34296 22.5093 6.88051 22.5093 7.42941C22.5093 7.97662 22.7326 8.51398 23.1206 8.902C23.5068 9.28813 24.0443 9.51148 24.5913 9.51148C25.1402 9.51148 25.6759 9.28813 26.0639 8.902C26.4519 8.51398 26.6734 7.97643 26.6734 7.42941C26.6734 6.88051 26.4519 6.34296 26.0639 5.95683C25.6778 5.56881 25.1402 5.34735 24.5913 5.34735Z" />
                </svg>

                <svg
                  className="facebook"
                  width="17"
                  height="32"
                  viewBox="0 0 17 32"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M13.3295 5.31333H16.2508V0.225333C15.7468 0.156 14.0135 0 11.9948 0C7.78284 0 4.89751 2.64933 4.89751 7.51867V12H0.249512V17.688H4.89751V32H10.5962V17.6893H15.0562L15.7642 12.0013H10.5948V8.08267C10.5962 6.43867 11.0388 5.31333 13.3295 5.31333Z" />
                </svg>
              </div>
            </div>
          </div>
          <div className="col-xl-3"></div>
          <div className="col-xl-2">
            <div className="roomsSec">
              <ul>
                <li>
                  <a href="#">Rooms</a>
                </li>
                <li>
                  <a href="#">Single Room</a>
                </li>
                <li>
                  <a href="#">Single Room</a>
                </li>
                <li>
                  <a href="#">Cabin</a>
                </li>
                <li>
                  <a href="#">Custom Room</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-xl-2">
            <div className="reservationSec">
              <ul>
                <li>
                  <a href="#">Reservation</a>
                </li>
                <li>
                  <a href="#">See the Steps</a>
                </li>
                <li>
                  <a href="#">Best Time</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-xl-2">
            <div className="contactSec">
              <ul>
                <li>
                  <a href="#">Contact</a>
                </li>
                <li>
                  <a href="#">Our Number</a>
                </li>
                <li>
                  <a href="#">Our Email</a>
                </li>
                <li>
                  <a href="#">Our Location</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>,
  document.getElementById("root")
);
